<?php

$router
    ->namespace('Client')
    ->prefix('clients')
    ->group(function() use ($router) {
        $router
            ->prefix('orders')
            ->group(function() use($router) {
            // Create order
            $router
                ->post('/', 'OrderController@store')
                ->middleware('can:store, App\Order');
            // Get order
            $router
                ->get('{order}', 'OrderController@show')
                ->middleware('can:show, App\Order,order');
            // Get clients orders
            $router
                ->get('/', 'OrderController@getClientsOrders')
                ->middleware('can:getClientOrders,App\Order');
            // Apply diagnose_at
            $router
                ->post('{order}/diagnose_at/apply', 'OrderController@applyDiagnoseAt')
                ->middleware('can:orderBelongsToClient, App\Order,order');
            // Reject
            $router
                ->post('{order}/reject', 'OrderController@reject')
                ->middleware('can:orderBelongsToClient, App\Order,order');
            // Apply defect act
            $router
                ->post('{order}/defect-acts/{defectAct}/apply', 'DefectActController@apply')
                ->middleware('can:defectActBelongsToClient, App\DefectAct,defectAct,order');
            // Reject defect act
            $router
                ->post('{order}/defect-acts/{defectAct}/reject', 'DefectActController@reject')
                ->middleware('can:defectActBelongsToClient, App\DefectAct,defectAct,order');
            
            // Update part from defect act
            $router
                ->put('{order}/defect-acts/parts/{defectPart}', 'DefectPartAPIController@update')
                ->middleware('can:updatePart, App\DefectAct');
            // Update part from defect act
            $router
                ->put('{order}/defect-acts/works/{defectWork}', 'DefectWorkAPIController@update')
                ->middleware('can:updateWork, App\DefectAct');
            });
        
        // Get clients cars
        $router
            ->get('cars', 'CarController@index')
            ->middleware('can:getClientsCars, App\Car');
        // Get clients contracts
        $router
            ->get('contracts', 'ContractController@index')
            ->middleware('can:getClientsContracts, App\Contract');
        // Get clients drivers
        $router
            ->get('drivers', 'DriverController@index')
            ->middleware('can:getClientsDrivers, App\Driver');
    });