<?php

$router
    ->namespace('Op')
    ->prefix('ops')
    ->group(function() use($router) {
        $router
            ->prefix('orders')
            ->group(function() use($router) {
                // ORPS orders
                $router
                    ->get('/', 'OrderController@getOrders');
                    // middleware
                $router
                    ->get('{order}', 'OrderController@getOrder');
                    // middleware
                $router
                    ->post('{order}/defect-acts/{defectAct}/apply', 'DefectActController@sendToAccountant');
            });
    });