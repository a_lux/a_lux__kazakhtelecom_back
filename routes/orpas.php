<?php
$router
    ->namespace('Orpa')
    ->prefix('orpas')
    ->group(function() use($router) {
        $router
            ->prefix('orders')
            ->group(function() use($router) {
                // Orpa's orders
                $router
                    ->get('orders', 'OrderController@getOrpaOrders')
                    ->name('get.orpa.orders')
                    ->middleware('can:getOrpaOrders, App\Order');
                // Get order
                $router
                    ->get('{order}', 'OrderController@show')
                    ->middleware('can:show, App\Order,order');
                // Update deffect act
                $router
                    ->put('{order}/defect-acts/{defectAct}', 'DefectActController@update')
                    ->name('updateByOrpa.defect.act')
                    ->middleware('can:App\DefectAct,update,defectAct');
                // Attach deffect act
                $router
                    ->post('{order}/defect-acts', 'DefectActController@store')
                    ->name('store.defect.act')
                    ->middleware('can:store,App\DefectAct,order');
                // Update deffect act
                $router
                    ->put('{order}/defect-acts/{defectAct}', 'DefectActController@update')
                    ->name('store.defect.act')
                    ->middleware('can:update,App\DefectAct,order,defectAct');
                // Approve deffect act
                $router
                    ->post('{order}/defect-acts/{defectAct}/approve', 'OrderController@approveDefectAct')
                    ->name('approveOrpa.defect.act');
                // Attach files
                $router
                    ->post('{order}/attach-files', 'OrderController@attachFiles')
                    ->name('attach.files')
                    ->middleware('can:attachFilesToOrder, App\Order,order');
            });
        // Create a new shop
        $router
            ->post('shops/store', 'ShopController@store')
            ->name('store.shops')
            ->middleware('can:store, App\Shop');
        $router
            ->get('shops', 'ShopController@index');
        $router
            ->get('shops/{shop}', 'ShopController@show');
    });