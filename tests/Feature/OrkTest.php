<?php

namespace Tests\Feature;

use App\User;
use App\Order;
use App\Company;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrkTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testOrkCanApplyAnOrder() {
        $user = User::where('role_id', 1)->first();
        $order = Order::where('status_internal', 0)->whereNull('ork_id')->first();
        
        $response = $this->actingAs($user, 'api')->json('POST', 'api/orders/apply/'.$order->id);

        $response->assertSuccessful();
    }

    public function testOrkCanCreateACompany() {
        $faker = \Faker\Factory::create();
        $user = User::where('role_id', 1)->first();
        $data = [
            'name' => $faker->company,
            'jur_address' => $faker->streetAddress,
            'fact_address' => $faker->streetAddress,
            'bin' => $faker->isbn13,
            'bik' => $faker->isbn13,
            'iik' => $faker->isbn13,
            'contact_person' => $faker->firstName,
            'contact_phone' => $faker->tollFreePhoneNumber
        ];
        $response = $this->actingAs($user, 'api')->json('POST', 'api/orks/companies/store', $data);
        return $response->assertStatus(200);
    }

    public function testOrkCanCreateADriver() {
        $faker = \Faker\Factory::create();
        $user = User::where('role_id', 1)->first();
        $company = Company::first();
        
        $data = [
            'phone' => $faker->tollFreePhoneNumber,
            'name' => $faker->firstName,
            'company_id' => $company->id
        ];

        $response = $this->actingAs($user, 'api')->json('POST', 'api/orks/drivers/store', $data);
        return $response->assertStatus(200);
    }
}
