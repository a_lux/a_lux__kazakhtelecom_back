<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testUserCanLogIn()
    {
        $user = User::first();
        $data = [
            'email' => $user->email,
            'password' => 'secret'
        ];

        $response = $this->json('POST', 'api/login', $data);
        $response->assertStatus(200);
    }

    public function testUserCannotLoginWithFakePassword() {
        $user = User::first();
        $data = [
            'email' => $user->email,
            'password' => 'secretsecret'
        ];

        $response = $this->json('POST', 'api/login', $data);
        $response->assertStatus(422);
    }

    public function testUserCanLogout() {
        $user = User::first();

        $response = $this->actingAs($user, 'api')->json('POST', '/api/logout');

        $response->assertSuccessful();
    }
}
