<?php

namespace Tests\Feature;

use App\User;
use App\Order;
use Tests\TestCase;
use App\StationManager;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StationTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testStationCanSeeItsOrders() {
        $this->withoutExceptionHandling();
        $user = User::find(StationManager::first()->user_id);
        $response = $this->actingAs($user, 'api')->json('GET', 'api/stations/orders');

        $response->assertSuccessful();
    }

    public function testStationCanCreateDefectAct() {
        $stationManager = StationManager::first();
        $user = User::find($stationManager->user_id);
        $order = Order::where('station_id', $stationManager->station_id)->where('status_internal', 2)->first();
       
        $data = [
            'parts' => [
                '1' => [
                    'part_id' => 1,
                    'amount' => 3,
                    'price' => 1500
                ]
            ],
            'works' => [
                '1' => [
                    'work_id' => 1,
                    'amount' => 4,
                    'price' => 5000
                ]
            ],
            'order_id' => $order->id
        ];

        $response = $this->actingAs($user, 'api')->json('POST', 'api/stations/defect-acts/store', $data);

        $response->assertSuccessful();
    }

    public function testStationCanApproveAnOrder() {
        $stationManager = StationManager::first();
        $user = User::find($stationManager->user_id);
        $order = Order::where('station_id', $stationManager->station_id)->where('status_internal', 2)->first();
        

        $response = $this->actingAs($user, 'api')->json('POST', 'api/stations/orders/approve/'.$order->id);

        $response->assertSuccessful();
    }
}
