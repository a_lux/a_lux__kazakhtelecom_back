<?php

namespace Tests\Feature;

use App\User;
use App\Order;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrpaTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testOrpaCanGetAvailableOrders() {
        $user = User::where('role_id', 4)->first();
        
        $response = $this->actingAs($user, 'api')->json('GET', 'api/orpas/orders/available');

        $response->assertSuccessful();
    }

    public function testOrpaCanGetItsOrders() {
        $user = User::where('role_id', 4)->first();
        
        $response = $this->actingAs($user, 'api')->json('GET', 'api/orpas/orders');

        $response->assertSuccessful();
    }

    public function testOrpaCanApplyAnOrder() {
        $user = User::where('role_id', 4)->first();
        $order = Order::where('status_internal', 4)->whereNull('orpa_id')->first();

        $response = $this->actingAs($user, 'api')->json('POST', 'api/orpas/orders/apply/'.$order->id);

        $response->assertSuccessful();
    }

    public function testOrpaCanUpdateDefectAct() {

        $this->withoutExceptionHandling();
        $user = User::where('role_id', 4)->first();
        $order = Order::where('status_internal', 5)->where('orpa_id', $user->id)->first();
        $defectAct = $order->defectActs[0];
        $data = [
            'parts' => [
                '1' => [
                    'part_id' => 1,
                    'amount' => 5,
                    'price' => 500
                ],
                '2' => [
                    'part_id' => 2,
                    'amount' => 5,
                    'price' => 500
                ]
            ],
            'works' => [
                '1' => [
                    'work_id' => 1,
                    'amount' => 5,
                    'price' => 500
                ],
                '2' => [
                    'work_id' => 2,
                    'amount' => 5,
                    'price' => 500
                ]
            ]
        ];

        $response = $this->actingAs($user, 'api')->json('POST', 'api/orpas/defect-acts/update/'.$defectAct->id, $data);
        
        $response->assertSuccessful();
    }

    public function testOrpaCanCreateAShop() {
        $this->withoutExceptionHandling();
        $faker = \Faker\Factory::create();
        $user = User::where('role_id', 4)->first();
        $data = [
            'shop_name' => $faker->company,
            'city' => $faker->city,
            'address' => $faker->streetAddress,
            'dir_name' => $faker->name,
            'dir_phone' => $faker->phoneNumber,
            'specialist_name' => $faker->name,
            'specialist_phone' => $faker->phoneNumber,
            'jur_address' => $faker->streetAddress,
            'fact_address' => $faker->streetAddress,
            'bin' => $faker->isbn13,
            'bik' => $faker->isbn13,
            'iik' => $faker->isbn13,
            'contact_person' => $faker->name,
            'contact_phone' => $faker->phoneNumber
        ];
        $response = $this->actingAs($user, 'api')->json('POST', '/api/orpas/shops/store', $data);
        
        $response->assertSuccessful();
    }

    public function testOrpaCanApproveDefectAct() {
        $user = User::where('role_id', 4)->first();
        $order = Order::where('status_internal', 5)->where('orpa_id', $user->id)->first();
        $response = $this->actingAs($user, 'api')->json('POST', '/api/orpas/orders/approve/'.$order->id);
        
        $response->assertSuccessful();
    }
}
