<?php

namespace App\Policies;

use App\User;
use App\Order;
use App\DefectAct;
use App\UserOrder;
use App\DefectPart;
use App\DefectWork;
use Illuminate\Http\Request;
use Illuminate\Auth\Access\HandlesAuthorization;

class DefectActPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    // Client
    public function defectActBelongsToClient(User $user,$defectAct, $order = null) {
        $order = is_string($order) ? Order::where('id', $order)->first() : $order;
        $defectAct = is_string($defectAct) ? DefectAct::where('id', $defectAct)->first() : $defectAct;
        
        if($defectAct->order_id == $order->id) {
            return $defectAct->order->client_id == $user->client->id;
        }

        return false;
    }
    // Ork
    public function defectActBelongsToOrk(User $user, DefectAct $defectAct) {
        $order_id = $defectAct->order->id;
        return UserOrder::where('user_id', $user->id)->where('order_id', $order_id)->exists();
    }
    public function defectPartBelongsToOrk(User $user, DefectPart $defectPart) {
        $order_id = $defectPart->defectAct->order->id;
        return UserOrder::where('user_id', $user->id)->where('order_id', $order_id)->exists();
    }
    public function defectWorkBelongsToOrk(User $user, DefectWork $defectWork) {
        $order_id = $defectWork->defectAct->order->id;
        return UserOrder::where('user_id', $user->id)->where('order_id', $order_id)->exists();
    }
    
    public function destroyPart(User $user) {
        return $user->role_id == 1;
    }
    public function updatePart(User $user) {
        return $user->role_id == 1 || $user->role_id == 2;
    }
    public function updateWork(User $user) {
        return $user->role_id == 1 || $user->role_id == 2;
    }
    public function destroyWork(User $user) {
        return $user->role_id == 1;
    }
    // Station
    public function store(User $user, Order $order) {
        if($user->role_id == 4 || $user->role_id == 5) {
            return UserOrder::where('user_id', $user->id)->where('order_id', $order->id)->exists();
        }
        return false;
    }

    public function defectActBelongsToStation(User $user, Order $order, DefectAct $defectAct) {
        $order_id = $defectAct->order->id;
        return UserOrder::where('user_id', $user->id)->where('order_id', $order_id)->exists();
    }

    public function update(User $user, $defectAct, $order) {
        if($defectAct->rejected || $defectAct->order_id != $order->id) {
            return false;
        }else {
            if($user->role_id == 4) {
                return $order->orpa_id == $user->id;
            }elseif($user->role_id == 5) {
                return $order->station_id == $user->stationManager->station_id;
            }
            return false;
        }
    }
}
