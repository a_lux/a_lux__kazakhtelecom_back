<?php

namespace App\Providers;

use App\Car;
use App\Order;
use App\Driver;
use App\Company;
use App\Station;
use App\DefectAct;
use App\Evacuator;
use App\Policies\CarPolicy;
use App\Policies\OrderPolicy;
use App\Policies\DriverPolicy;
use Laravel\Passport\Passport;
use App\Policies\CompanyPolicy;
use App\Policies\StationPolicy;
use App\Policies\DefectActPolicy;
use App\Policies\EvacuatorPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    
    protected $policies = [
        Order::class => OrderPolicy::class,
        Company::class => CompanyPolicy::class,
        Station::class => StationPolicy::class,
        Evacuator::class => EvacuatorPolicy::class,
        Driver::class => DriverPolicy::class,
        DefectAct::class => DefectActPolicy::class,
        Car::class => CarPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //Passport

        Passport::routes();
    }
}
