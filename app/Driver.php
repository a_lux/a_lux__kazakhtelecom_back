<?php

namespace App;

use App\Traits\ReturnsFillables;
use App\Traits\ReturnsColumnsByRole;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Driver extends Model
{
    use ReturnsFillables, SoftDeletes, ReturnsColumnsByRole;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phone', 'name', 'company_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'phone' => 'string',
        'name' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    static protected $columns = [
        'phone' => ['ork'],
        'name' => ['ork']
    ];
    /**
     * Get the Orders for the Driver.
     */
    public function orders()
    {
        return $this->hasMany(\App\Order::class);
    }

}
