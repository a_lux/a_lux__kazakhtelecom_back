<?php

namespace App;

use App\Traits\ReturnsFillables;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CarType extends Model
{
    use ReturnsFillables, SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        //
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    /**
     * Get the Stations for the CarType.
     */
    public function stations()
    {
        return $this->belongsToMany(\App\Station::class);
    }

    /**
     * Get the Shops for the CarType.
     */
    public function shops()
    {
        return $this->belongsToMany(\App\Shop::class);
    }

}
