<?php

namespace App;

use App\Traits\ReturnsFillables;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Station extends Model
{
    use ReturnsFillables, SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'city', 'address', 'fact_address', 'jur_address', 'bin', 'bik', 'iik', 'contact_person', 'contact_phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'city' => 'string',
        'address' => 'string',
        'jur_address' => 'string',
        'bin' => 'string',
        'bik' => 'string',
        'iik' => 'string',
        'contact_person' => 'string',
        'contact_phone' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    /**
     * Get the Orders for the Station.
     */
    public function orders()
    {
        return $this->hasMany(\App\Order::class);
    }


    /**
     * Get the CarTypes for the Station.
     */
    public function carTypes()
    {
        return $this->belongsToMany(\App\CarType::class);
    }

}
