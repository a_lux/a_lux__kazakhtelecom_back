<?php

namespace App;

use App\Traits\ReturnsFillables;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DefectPart extends Model
{
    use ReturnsFillables, SoftDeletes;
    protected $table = 'defect_act_part';

    protected $fillable = [
        'amount',
        'price',
        'price_changed_at',
        'price_with_markup',
        'price_with_markup_updated_at',
        'applied',
        'applied_at',
        'paid',
        'paid_at',
        'part_id',
        'defect_act_id'];

    public function defectAct() {
        return $this->belongsTo(\App\DefectAct::class);
    }
}
