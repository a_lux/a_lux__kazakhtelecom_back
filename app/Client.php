<?php

namespace App;

use App\Traits\ReturnsFillables;
use App\Traits\ReturnsColumnsByRole;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use ReturnsFillables, SoftDeletes, ReturnsColumnsByRole;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'address', 'phone', 'email', 'company_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'address' => 'string',
        'phone' => 'string',
        'email' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];
    
    static protected $columns = [
        'name' => ['ork'],
        'phone' => ['ork'],
        'email' => ['ork']
    ];
    /**
     * Get the Orders for the Client.
     */
    public function orders()
    {
        return $this->hasMany(\App\Order::class);
    }


    /**
     * Get the User for the Client.
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }


    /**
     * Get the Company for the Client.
     */
    public function company()
    {
        return $this->belongsTo(\App\Company::class);
    }

}
