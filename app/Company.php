<?php

namespace App;

use App\Traits\ReturnsFillables;
use App\Traits\ReturnsColumnsByRole;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
    use ReturnsFillables, SoftDeletes, ReturnsColumnsByRole;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'jur_address', 'fact_address', 'bin', 'bik', 'iik', 'contact_person', 'contact_phone', 'region_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'jur_address' => 'string',
        'bin' => 'string',
        'bik' => 'string',
        'iik' => 'string',
        'contact_person' => 'string',
        'contact_phone' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];
    
    static protected $columns = [
        'id' => ['ork'],
        'name' => ['ork'],
        'jur_address' => ['ork'],
        'fact_address' => ['ork'], 
        'bin' => ['ork'],
        'bik' => ['ork'],
        'iik' => ['ork'],
        'contact_person' => ['ork'],
        'contact_phone' => ['ork'],
        'region' => ['ork'],
        'created_at' => ['ork']
    ];
    /**
     * Get the Clients for the Company.
     */
    public function client()
    {
        return $this->hasOne(\App\Client::class);
    }


    /**
     * Get the Cars for the Company.
     */
    public function cars()
    {
        return $this->hasMany(\App\Car::class);
    }

}
