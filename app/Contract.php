<?php

namespace App;

use App\Traits\ReturnsColumnsByRole;
use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    use ReturnsColumnsByRole;
    protected $fillable = ['company_id', 'number_of_сontract', 'signed_at', 'expire_at'];

    static protected $columns = [
        'id' => ['ork'],
        'number_of_сontract' => ['ork'],
        'signed_at' => ['ork'],
        'expire_at' => ['ork']
    ];
}
