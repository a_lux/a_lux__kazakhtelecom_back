<?php

namespace App\Traits;

trait ReturnsFillables {
    public function getFillables() {
        return $this->fillable;
    }
}