<?php

namespace App\Traits;

trait ReturnsColumnsByRole {
    static public function getColumns($role = null) {
        if($role) {
            $cols = [];
            foreach(static::$columns as $idx => $columns) {
                if(in_array($role, $columns)) {
                    $cols[] = $idx;
                }
            }
            return $cols;
        }
        return array_keys(static::$columns);
    }

    static function getColumn($col) {
        $cols = array_keys(static::$columns);

        return $cols[array_search('defect_acts', array_keys(static::$columns))];
    }
}