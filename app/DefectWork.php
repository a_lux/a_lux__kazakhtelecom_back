<?php

namespace App;

use App\Traits\ReturnsFillables;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DefectWork extends Model
{
    use ReturnsFillables, SoftDeletes;
    protected $table = 'defect_act_work';
    protected $fillable = [
        'amount',
        'applied',
        'applied_at',
        'price',
        'price_with_markup',
        'price_with_markup_updated_at',
        'price_changed_at',
        'defect_act_id',
        'work_id'];
    
    public function defectAct() {
        return $this->belongsTo(\App\DefectAct::class);
    }
}
