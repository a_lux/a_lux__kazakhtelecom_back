<?php

namespace App;

use App\Traits\ReturnsFillables;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DefectAct extends Model
{
    use ReturnsFillables, SoftDeletes;

    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 'paid', 'paid_at', 'rejected', 'rejected_at', 'markup_percent'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'paid' => 'boolean',
        'paid_at' => 'timestamp',
        'rejected' => 'boolean',
        'rejected_at`' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp'
    ];

    /**
     * Get the Order for the DefectAct.
     */
    public function order()
    {
        return $this->belongsTo(\App\Order::class);
    }

    /**
     * Get the Parts for the DefectAct.
     */
    public function parts()
    {
        return $this->belongsToMany(\App\Part::class);
    }
    /**
     * Get the Works for the DefectAct.
     */
    public function works()
    {
        return $this->belongsToMany(\App\Work::class);
    }

}
