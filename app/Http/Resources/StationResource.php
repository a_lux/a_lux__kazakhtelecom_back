<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'city' => $this->city,
            'address' => $this->address,
            'fact_address' => $this->fact_address,
            'jur_address' => $this->jur_address,
            'bin' => $this->bin,
            'bik' => $this->bik,
            'iik' => $this->iik,
            'contact_person' => $this->contact_person,
            'contact_phone' => $this->contact_phone,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'orders' => OrderResource::collection($this->whenLoaded('orders')),
            'car_types' => CarTypeResource::collection($this->whenLoaded('car_types'))
        ];
    }
}
