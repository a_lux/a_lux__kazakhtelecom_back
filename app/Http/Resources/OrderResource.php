<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'car_id' => $this->car_id,
            'client_id' => $this->client_id,
            'is_evacuated' => $this->is_evacuated,
            'location' => $this->location,
            'driver_id' => $this->driver_id,
            'problem_description' => $this->problem_description,
            'status' => $this->status,
            'status_internal' => $this->status_internal,
            'ready_to_repair_at' => $this->ready_to_repair_at,
            'station_id' => $this->station_id,
            'completed_at' => $this->completed_at,
            'evacuator_id' => $this->evacuator_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'defect_acts' => DefectActResource::collection($this->whenLoaded('defect_acts')),
            'comments' => CommentResource::collection($this->whenLoaded('comments')),
            'media' => MediaResource::collection($this->whenLoaded('media')),
            'client' => new ClientResource($this->whenLoaded('client')),
            'driver' => new DriverResource($this->whenLoaded('driver')),
            'car' => new CarResource($this->whenLoaded('car')),
            'evacuator' => new EvacuatorResource($this->whenLoaded('evacuator')),
            'station' => new StationResource($this->whenLoaded('station'))
        ];
    }
}
