<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DefectActResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order_id' => $this->order_id,
            'paid' => $this->paid,
            'paid_at' => $this->paid_at,
            'rejected' => $this->rejected,
            'rejected_at' => $this->rejected_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'parts' => PartResource::collection($this->whenLoaded('parts')),
            'works' => WorkResource::collection($this->whenLoaded('works')),
        ];
    }
}
