<?php

namespace App\Http\Controllers;

use App\CarType;
use App\Http\Resources\CarTypeCollection;
use App\Http\Resources\CarTypeResource;
 
class CarTypeAPIController extends Controller
{
    public function index()
    {
        return new CarTypeCollection(CarType::paginate());
    }
 
    public function show(CarType $carType)
    {
        return new CarTypeResource($carType->load(['stations', 'shops']));
    }

    public function store(Request $request)
    {
        return new CarTypeResource(CarType::create($request->all()));
    }

    public function update(Request $request, CarType $carType)
    {
        $carType->update($request->all());

        return new CarTypeResource($carType);
    }

    public function destroy(Request $request, CarType $carType)
    {
        $carType->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
