<?php

namespace App\Http\Controllers\Api\Client;

use App\Driver;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DriverController extends Controller
{
    public function index(Request $request) {
        $drivers = Driver::where('company_id', $request->user()->client->company_id)->get();

        return response($drivers, 200);
    }
}
