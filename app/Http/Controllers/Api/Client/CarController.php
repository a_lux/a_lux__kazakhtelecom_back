<?php

namespace App\Http\Controllers\Api\Client;

use App\Car;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CarController extends Controller
{
    public function index(Request $request) {
        $cars = Car::where('company_id', $request->user()->client->company_id)->get();

        return response($cars, 200);
    }
}
