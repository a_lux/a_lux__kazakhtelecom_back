<?php

namespace App\Http\Controllers\Api\Client;

use App\Order;
use App\Status;
use App\DefectAct;
use App\DefectActInProcess;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Templates\OrderTemplate;
use App\Notifications\ClientRejectedAnOrder;
use App\Notifications\ClientAppliedDefectAct;

class DefectActController extends Controller
{
    public function apply(Request $request, Order $order, DefectAct $defectAct) {
        $client = $order->client;
        $company = $client->company;
        $status_id = Status::where('name', 'Ожидает назначения ремонта от СТО')->first()->id;

        $order = OrderTemplate::init($order)
            ->changeStatus(11)
            ->changeStatus($status_id, false)
            ->getOrder();
        
        DefectActInProcess::where('defect_act_id', $defectAct->id)
            ->where('order_id', $order->id)->first()->fill(['approved' => 1])->first();

        $orkManager = User::find($order->ork_id);
        $orpuManager = User::find($order->orpu_id);

        $orkManager->notify(new ClientAppliedDefectAct($client->name, $company->name, $order->id, $defectAct->id));
        $orpuManager->notify(new ClientAppliedDefectAct($client->name, $company->name, $order->id, $defectAct->id));

        return response(['status_changed' => $order->status_internal] ,200);
    }
    
    public function approveDefectActWhileRepairing(Request $request, Order $order) {
        $status_id = Status::where('name', 'Проводятся ремонтные работы')->first()->id;
        $order = OrderTemplate::init($order)
            ->changeStatus(17)
            ->changeStatus($status_id, false)
            ->getOrder();

        return response(['status_changed' => $order->status_internal] ,200);
    }

    public function reject(Request $request, Order $order, DefectAct $defectAct) {
        if($order->status_internal != 16) {
            $client = $order->client;
            $company = $client->company;
            $defectAct->fill([
                'reject' => 1,
                'rejected_at' => DB::raw('now()')
            ])->save();
            
            $order = OrderTemplate::init($order)
                ->changeStatus(12)
                ->getOrder();

            $orkManager = User::find($order->ork_id);
            $orpuManager = User::find($order->orpu_id);
            
            if($order->station_id) {
                $stationManagerId = StationManager::where('station_id', $order->station_id)->first()->user_id;
                $stationManager = User::find($stationManagerId);    
                $stationManager->notify(new ClientRejectedAnOrder($client->name, $company->name, $order->id));
            }
            $orkManager->notify(new ClientRejectedAnOrder($client->name, $company->name, $order->id));
            $orpuManager->notify(new ClientRejectedAnOrder($client->name, $company->name, $order->id));
            
            return response(['status_changed' => $order->status_internal] ,200);
        }
        return response(['message' => 'Reject is not allowed'], 403);
    }
}
