<?php

namespace App\Http\Controllers\Api\Client;

use App\Contract;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContractController extends Controller
{
    public function index(Request $request) {
        $contracts = Contract::where('company_id', $request->user()->client->company_id)->get();

        return response($contracts, 200);
    }
}
