<?php

namespace App\Http\Controllers\Api\Client;

use App\Part;
use App\DefectPart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DefectPartController extends Controller
{
    public function apply(Request $request, DefectPart $defectPart) {
        if($defectPart->applied == 0) {
            $defectPart->fill([
                'applied' => 1,
                'applied_at' => DB::raw('now()')
            ]);
        }else {
            $defectPart->fill([
                'applied' => 0,
                'applied_at' => null
            ]);
        }
        
        $defectPart->save();

        return response($defectPart->applied_at, 200);
    }
    
    public function update(Request $request, DefectPart $defectPart) {
        if(!empty($request->all())) {
            $defectPart
                ->fill($request->all())
                ->save();
        }

        return response($defectPart, 200);
    }
}
