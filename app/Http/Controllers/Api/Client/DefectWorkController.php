<?php

namespace App\Http\Controllers\Api\Client;

use App\DefectWork;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DefectWorkController extends Controller
{
    public function apply(Request $request, DefectWork $defectWork) {
        if($defectWork->applied == 0) {
            $defectWork->fill([
                'applied' => 1,
                'applied_at' => DB::raw('now()')
            ]);
        }else {
            $defectWork->fill([
                'applied' => 0,
                'applied_at' => null
            ]);
        }
        
        $defectWork->save();

        return response($defectWork->applied_at, 200);
    }

    public function update(Request $request, DefectWork $defectWork) {
        if(!empty($request->all())) {
            $defectWork
                ->fill($request->all())
                ->save();
        }

        return response($defectWork, 200);
    }
}
