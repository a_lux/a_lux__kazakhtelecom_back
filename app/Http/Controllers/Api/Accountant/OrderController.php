<?php

namespace App\Http\Controllers\Api\Accountant;

use App\Order;
use App\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function index() {

    }

    public function show() {

    }

    public function paid(Request $request, Order $order, DefectAct $defectAct) {
        $parent = Status::where('name', 'Ремонт выполнен')->first()->id;
        $status_id = Status::where('name', 'Оплата')->where('parent_id', $parent)->first()->id;

        $order = OrderTemplate::init($order)
            ->changeStatus(21)
            ->changeStatus($status_id, false)
            ->getOrder();
        
        $defectAct->fill([
            'paid' => 1,
            'paid_at' => $request->paid_at
        ])->save();

        return response(['status' => $order->status_internal], 200);
    }
}
