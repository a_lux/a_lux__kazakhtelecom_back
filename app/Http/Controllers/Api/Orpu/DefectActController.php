<?php

namespace App\Http\Controllers\Api\Orpu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DefectActController extends Controller
{
    public function reject(Request $request, DefectAct $defectAct) {
        $defectAct->fill([
            'rejected' => 1,
            'rejected_at' => DB::raw('now()')
        ])->save();
        
        $defectAct->order->fill([
            'station_id' => null,
            'ready_to_diagnose_at' => null,
            'status_internal' => 2
        ])->save();

        return response($defectAct, 200);
    }
}
