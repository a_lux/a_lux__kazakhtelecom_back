<?php

namespace App\Http\Controllers\Api\Orpu;

use App\User;
use App\Order;
use App\Status;
use App\StationManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Templates\UserTemplate;
use App\Http\Templates\OrderTemplate;

class OrderController extends Controller
{
    public function show(Request $request, $id)
    {
        $order = $request
            ->user()
            ->orders()
            ->select([
                'orders.id', 
                DB::raw('CONCAT(cars.brand, " ",cars.model) AS car_name'), 
                'cars.number as car_number', 'orders.location', 
                'orders.problem_description', 'is_evacuated', 
                'status_internal', 
                'stations.name as station_name', 
                'stations.address', 
                'stations.contact_person', 
                'stations.contact_phone',
                'evacuators.name as evacuator_name',
                'evacuators.phone as evacuator_phone'])
                ->join('cars', 'cars.id', 'orders.car_id')
                ->leftJoin('stations', 'stations.id', 'orders.station_id')
                ->leftJoin('evacuators', 'evacuators.id', 'orders.evacuator_id')
                ->with('defectActs', 'defectActs.parts', 'defectActs.works', 'chatroom.participants', 'chatroom.messages')
                ->where('orders.id', $id)
                ->first();
        
        $colNames = ['id',
        'client_name',
        'car_number',
        'problem_description',
        'station_name',
        'address',
        'contact_person',
        'contact_phone',
        'evacuator_name',
        'evacuator_phone'];
        
        return response(['order' => $order, 'columns' => $colNames], 200);
    }

    public function update(Request $request, Order $order) {
        
        $this->validate($request, [
            'station_id' => 'required|integer',
            'evacuator_id' => $order->is_evacuated ? 'required' : 'nullable'
        ]);

        $stationManagerId = StationManager::where('station_id', $request->station_id)->first()->user_id;
        $status_id = Status::where('name', 'Ожидает назначение диагностики от СТО')->first()->id;
        $status_prev = Status::where('name', 'Поиск партнера')->first()->id;

        $order
            ->fill([
                'station_id' => $request->station_id,   
                'evacuator_id' => $request->evacuator_id ?: null,
                'status' => $request->station_id ? $status_id : $status_prev,
                'status_internal' => $request->station_id ? 3 : 2
            ])->save();
        
        OrderTemplate::init($order)
            ->setOrUpdateUser($stationManagerId);
        
        return response($order, 200);
    }

    public function getOrders(Request $request, Order $order) {
        $orders = $request
            ->user()
            ->orders()
            ->select([
                'orders.id',
                DB::raw('CONCAT(cars.brand, " ",cars.model) AS car_name'), 
                'cars.number as car_number', 'orders.location', 
                'orders.problem_description', 'is_evacuated', 
                'status_internal', 
                'stations.name as station_name', 
                'stations.address', 
                'stations.contact_person', 
                'stations.contact_phone',
                'evacuators.name as evacuator_name',
                'evacuators.phone as evacuator_phone'])
            ->join('cars', 'cars.id', 'orders.car_id')
            ->leftJoin('stations', 'stations.id', 'orders.station_id')
            ->leftJoin('evacuators', 'evacuators.id', 'orders.evacuator_id')
            ->with('defectActs', 'defectActs.parts', 'defectActs.works')
            ->get();
        
        $colNames = ['id',
        'client_name',
        'car_number',
        'problem_description',
        'station_name',
        'address',
        'contact_person',
        'contact_phone',
        'evacuator_name',
        'evacuator_phone'];
        
        return response(['orders' => $orders, 'columns' => $colNames], 200);
        
    }

    public function approveDefectAct(Request $request, Order $order) {
        $status_id = Status::where('name', 'ДА на согласовании в отделе по работе с клиентами')->first()->id;
        $order = OrderTemplate::init($order)
            ->changeStatus(9)
            ->changeStatus($status_id, false)
            ->getOrder();
        
        return response(['status_changed' => $order->status_internal] ,200);
    }
    public function sendToOrpa(Order $order) {
        $company = $order->client->company;
        $status_id = Status::where('name', 'Поиск автозапчестей')->first()->id;

        $orpaManagerId = UserTemplate::init()->getUser(4, $company->region_id)->id;
        
        $order = OrderTemplate::init($order)
            ->changeStatus(8)
            ->changeStatus($status_id, false)
            ->setUser($orpaManagerId)
            ->getOrder();
        
        return response($order, 200);
    }

    public function apply(Request $request, Order $order) {
        // apply 11
    }

    public function reject(Request $request, Order $order) {
        // reject 12
    }
}