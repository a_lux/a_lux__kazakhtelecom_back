<?php

namespace App\Http\Controllers\Api\Station;

use App\Part;
use App\Order;
use App\DefectAct;
use App\DefectPart;
use App\DefectWork;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\DefectActResource;

class DefectActController extends Controller
{
    public function show(Order $order, DefectAct $defectAct)
    {
        return new DefectActResource($defectAct->load(['parts', 'works']));
    }
    public function store(Request $request, Order $order) {
        $this->validate($request, [
            'parts' => count($request->works) == 0 ? 'required|array|min:1' : 'nullable',
            'works' => count($request->parts) == 0 ? 'required|array|min:1' : 'nullable'
        ]);
        $defectAct = DefectAct::create([
            'order_id' => $order->id,
            'paid' => 0,
            'rejected' => 0
        ]);
        
        foreach($request->except('works')['parts'] as $part) {
            if(is_null($part['id'])) {
                $dbPart = Part::create([
                    'name' => $part['name'],
                    'photo' => '',
                    'provider' => isset($part['provider']) ?: null
                ]);
                DB::table('defect_act_part')
                    ->insert([
                        'amount' => $part['amount'],
                        'price' => $part['price'],
                        'part_id' => $dbPart->id,
                        'defect_act_id' => $defectAct->id,
                        'applied' => 0,
                        'paid' => 0,
                        'user_id' => $request->user()->id
                    ]);
            }else {
                DB::table('defect_act_part')
                    ->insert([
                        'amount' => $part['amount'],
                        'price' => $part['price'],
                        'part_id' => $part['id'],
                        'defect_act_id' => $defectAct->id,
                        'applied' => 0,
                        'paid' => 0,
                        'user_id' => $request->user()->id
                    ]);
            }
        }
        
        foreach($request->except('parts')['works'] as $work) {
            DB::table('defect_act_work')
                    ->insert([
                        'amount' => $work['amount'],
                        'price' => $work['price'],
                        'work_id' => $work['id'],
                        'defect_act_id' => $defectAct->id,
                        'applied' => 0,
                        'user_id' => $request->user()->id
                    ]);
        }

        return response(['defect_act_id' => $defectAct->id], 200);
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'parts' => count($request->works) == 0 ? 'required|array|min:1' : 'nullable',
            'works' => count($request->parts) == 0 ? 'required|array|min:1' : 'nullable'
        ]);
        
        $defectAct = DefectAct::where('id', $id)->first();

        foreach($request->except('works')['parts'] as $part) {
            if(is_null($part['id'])) {
                $dbPart = Part::create([
                    'name' => $part['name'],
                    'photo' => '',
                    'provider' => isset($part['provider']) ?: null
                ]);
                DefectPart::create([
                        'amount' => $part['amount'],
                        'price' => $part['price'],
                        'part_id' => $dbPart->id,
                        'defect_act_id' => $defectAct->id,
                        'applied' => 0,
                        'paid' => 0
                    ]);
            }else {
                $defectPart = DefectPart::find($part['id']);
                $defectPart->fill([
                        'amount' => $part['amount'],
                        'price' => $part['price'],
                        'part_id' => $part['id'],
                        'defect_act_id' => $defectAct->id,
                    ])->save();
            }
        }
        
        foreach($request->except('parts')['works'] as $work) {
            DefectWork::find($work['id'])
                ->fill([
                    'amount' => $work['amount'],
                    'price' => $work['price'],
                    'work_id' => $work['id'],
                    'defect_act_id' => $defectAct->id,
                ])
                ->save();
        }

        return response(['defect_act_id' => $defectAct->id], 200);
    }
}