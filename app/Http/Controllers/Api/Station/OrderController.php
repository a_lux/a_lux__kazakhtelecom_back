<?php

namespace App\Http\Controllers\Api\Station;

use App\User;
use App\Media;
use App\Order;
use App\OpBill;
use App\Status;
use App\Station;
use App\DefectAct;
use App\StationManager;
use App\DefectActInProcess;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Templates\UserTemplate;
use App\Http\Templates\OrderTemplate;
use App\Notifications\StationHasCompletedRepair;
use App\Notifications\StationSetReadyToRepairAtDate;

class OrderController extends Controller
{
    public function show(Request $request, $order)
    {
        $order = $order
            ->select(['orders.id', 'location', 'problem_description', DB::raw('CONCAT(cars.brand, " ",cars.model) AS car'), 'drivers.name as driver', 'status', 'status_internal', 'actual_location', 'car_id', 'driver_id'])
            ->join('cars', 'cars.id', 'orders.car_id')
            ->join('drivers', 'drivers.id', 'orders.driver_id')
            ->with(['defectActs.parts', 'defectActs.works', 'chatroom.participants', 'chatroom.messages'])
            ->where('station_id', $request->user()->stationManager->station_id)
            ->where('orders.id', $order)
            ->first();
        
        $colNames = ['id', 'location', 'problem_description', 'actual_location', 'car', 'driver'];
        return response(['order' => $order, 'columns' => $colNames], 200);
    }

    public function diagnoseAt(Request $request, Order $order) {
        $this->validate($request, [
            'ready_to_diagnose_at' => 'required|string'
        ]);
        
        $status_id = Status::where('name', 'Назначена диагностика')->first()->id;

        $order = OrderTemplate::init($order)
            ->changeStatus(4)
            ->changeStatus($status_id, false)
            ->getOrder();
        
        return response($order, 200);
        
    }

    public function repairAt(Request $request, Order $order) {
        $this->validate($request, [
            'ready_to_repair_at' => 'required|string'
        ]);

        $client = $order->client;
        $company = $client->company;

        $orkManager = UserTemplate::init()->getUser(1, $company->region_id);
        $orpuManager = UserTemplate::init()->getUser(4, $company->region_id);
        
        $station = Station::where('id', $order->station_id)->first();
        
        $status_id = Status::where('name', 'Проводятся ремонтные работы')->first()->id;
        
        $order = OrderTemplate::init($order)
            ->changeStatus(13)
            ->changeStatus($status_id, false)
            ->getOrder();
        
        $orkManager->notify(new StationSetReadyToRepairAtDate($station->name, $order->id));
        $orpuManager->notify(new StationSetReadyToRepairAtDate($client->name, $order->id));

        return response($order, 200);
        
    }

    public function getStationOrders(Request $request, Order $order) {
        $orders = $order
            ->select(['orders.id', 'location', 'problem_description', DB::raw('CONCAT(cars.brand, " ",cars.model) AS car'), 'drivers.name as driver', 'status', 'status_internal', 'actual_location', 'car_id', 'driver_id'])
            ->join('cars', 'cars.id', 'orders.car_id')
            ->join('drivers', 'drivers.id', 'orders.driver_id')
            ->with(['defectActs.parts', 'defectActs.works'])
            ->where('station_id', $request->user()->stationManager->station_id);
        
        if($request->status_internal) {
            $orders = $orders->where('status_internal', $request->status_internal);
        }

        $orders = $orders->get();
        $colNames = ['id', 'location', 'problem_description', 'actual_location', 'car', 'driver'];
        return response(['orders' => $orders, 'columns' => $colNames], 200);
    }

    public function approveDefectAct(Request $request, Order $order, DefectAct $defectAct) {
        $status_id = Status::where('name', 'ДА ожидает согласования в отделе по работе с партнерами')->first()->id;

        if($request->input('while-repairing')) {
            $order = OrderTemplate::init($order)
                ->changeStatus(15)
                ->changeStatus($status_id, false)
                ->getOrder();
            $defectAct = DefectActInProcess::where('defect_act_id', $defectAct->id)
                ->where('order_id', $order->id)
                ->first()
                ->fill([
                    'approved' => 0
                ])
                ->save();
        }else {
            $order = OrderTemplate::init($order)
                ->changeStatus(7)
                ->changeStatus($status_id, false)
                ->getOrder();
            DefectActInProcess::create([
                'user_id' => $request->user()->id,
                'defect_act_id' => $defectAct->id,
                'order_id' => $order->id
            ]);
        }
        
        return response(['status_changed' => $order->status_internal] ,200);
    }

    public function startRepair(Request $request, Order $order) {
        $status_id = Status::where('name', 'Проводятся ремонтные работы')->first()->id;

        $order = OrderTemplate::init($order)
            ->changeStatus(14)
            ->changeStatus($status_id, false)
            ->getOrder();

        return response(['status_changed' => $order->status_internal] ,200);
    }

    public function releaseCar(Request $request, Order $order) {

        $order = OrderTemplate::init($order)
            ->changeStatus(18)
            ->getOrder();
        
        return response(['status_changed' => $order->status_internal] ,200);
    }

    public function complete(Request $request, Order $order) {
        $parent = Status::where('name', 'Ремонт выполнен')->first()->id;
        $status_id = Status::where('name', 'Оплата')->where('parent_id', $parent)->first()->id;
        $order = OrderTemplate::init($order)
            ->changeStatus(19)
            ->changeStatus($status_id, false)
            ->getOrder();
        
        if($order->orpa_id) {
            $station = Station::find($order->station_id);
            $orpaManager = User::find($order->orpa_id);
            $orpaManager->notify(new StationHasCompletedRepair($station->name, $order->id));
        }

        return response(['status_changed' => $order->status_internal] ,200);
    }

    public function attachFiles(Request $request, Order $order) {
        $this->validate($request, [
            'images' => 'required',
            'videos' => 'required',
            'bill' => 'required'
        ]);
        $stationManager = StationManager::where('station_id', $order->station->id)->first();
        
        OrderTemplate::init($order)
            ->attachImages($request->images, $stationManager->user_id, 1)
            ->attachVideos($request->videos, $stationManager->user_id, 1)
            ->attachBill($request->bill, $stationManager->user_id, 1);
        
        return response('Files are stored', 200);
    }
}
