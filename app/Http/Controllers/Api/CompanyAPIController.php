<?php

namespace App\Http\Controllers\Api;

use App\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CompanyCollection;

class CompanyAPIController extends Controller
{
    public function index() {
        return new CompanyCollection(Company::get());
    }
}
