<?php

namespace App\Http\Controllers\Api\Orpa;

use App\Shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ShopResource;
use App\Http\Resources\ShopCollection;
 
class ShopController extends Controller
{
    public function index()
    {
        return new ShopCollection(Shop::get());
    }
 
    public function show(Shop $shop)
    {
        return new ShopResource($shop->load(['carTypes']));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'shop_name' => 'required|string',
            'city' => 'required|string',
            'address' => 'required|string',
            'dir_name' => 'required|string',
            'dir_phone' => 'required|string',
            'specialist_name' => 'required|string',
            'specialist_phone' => 'required|string',
            'jur_address' => 'required|string',
            'fact_address' => 'required|string',
            'bin' => 'required|string',
            'bik' => 'required|string',
            'iik' => 'required|string',
            'contact_person' => 'required|string',
            'contact_phone' => 'required|string'
        ]);
        
        $shop = Shop::create([
            'name' => $request->shop_name,
            'city' => $request->city,
            'address' => $request->address,
            'dir_name' => $request->dir_name,
            'dir_phone' => $request->dir_phone,
            'specialist_name' => $request->specialist_name,
            'specialist_phone' => $request->specialist_phone,
            'jur_address' => $request->jur_address,
            'fact_address' => $request->fact_address,
            'bin' => $request->bin,
            'bik' => $request->bik,
            'iik' => $request->iik,
            'contact_person' => $request->contact_person,
            'contact_phone' => $request->contact_phone
        ]);

        return response(new ShopResource($shop), 200);
    }

    public function update(Request $request, Shop $shop)
    {
        $shop->update($request->all());

        return new ShopResource($shop);
    }

    public function destroy(Request $request, Shop $shop)
    {
        $shop->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
