<?php

namespace App\Http\Controllers\Api\Op;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function index(Request $request) {
        $orders = $request
            ->user()
            ->orders()
            ->select([
                'orders.id',
                'clients.name as client_name',
                DB::raw('CONCAT(cars.brand, " ",cars.model) AS car_name'), 
                'cars.number as car_number',
                'orders.location', 
                'orders.problem_description',
                'status_internal',
                'evacuators.name as evacuator_name',
            ])
            ->join('cars', 'cars.id', 'orders.car_id')
            ->join('clients', 'clients.id', 'orders.client_id')
            ->leftJoin('evacuators', 'evacuators.id', 'orders.evacuator_id')
            ->with('defectActs', 'defectActs.parts', 'defectActs.works')
            ->get();
        
        $colNames = ['id', 'client name', 'car_name', 'number', 'location', 'address', 'evacuator_name'];
        return response(['orders' => $orders, 'columns' => $colNames], 200);
    }

    public function show(Request $request, $id) {
        $order = $request
        ->user()
        ->orders()
        ->select([
            'orders.id',
            'clients.name as client_name',
            DB::raw('CONCAT(cars.brand, " ",cars.model) AS car_name'), 
            'cars.number as car_number',
            'orders.location', 
            'orders.problem_description',
            'status_internal',
            'evacuators.name as evacuator_name',
        ])
        ->join('cars', 'cars.id', 'orders.car_id')
        ->join('clients', 'clients.id', 'orders.client_id')
        ->leftJoin('evacuators', 'evacuators.id', 'orders.evacuator_id')
        ->where('orders.id', $id)
        ->with('defectActs', 'defectActs.parts', 'defectActs.works')
        ->first();

        $colNames = ['id', 'client name', 'car_name', 'number', 'location', 'address', 'evacuator_name'];
        return response(['order' => $order, 'columns' => $colNames], 200);
    }
}
