<?php

namespace App\Http\Controllers\Api\Op;

use App\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Templates\OrderTemplate;

class DefectActController extends Controller
{
    public function apply(Request $request, Order $order, DefectAct $defectAct) {
        $parent = Status::where('name', 'Ремонт выполнен')->first()->id;
        $status_id = Status::where('name', 'Оплата')->where('parent_id', $parent)->first()->id;
        OrderTemplate::init($order)
            ->changeStatus(20)
            ->changeStatus($status_id, false);

        return response(['status' => $order->status], 200);
    }
}
