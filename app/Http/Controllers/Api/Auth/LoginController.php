<?php

namespace App\Http\Controllers\Api\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function login(Request $request) {
        $this->validate($request, [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string'
        ]);

        $user = User::where('email', $request->email)->first();
        
        if ($user) {
            if(Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                $response = ['token' => $token, 'role' => $user->role_id, 'user' => $user];
                return response($response, 200);
            }else {
                $response = "Password missmatch";
                return response($response, 422);
            }
        }else {
            $response = 'User does not exist';
            return response($response, 422);
        }
    }
}
