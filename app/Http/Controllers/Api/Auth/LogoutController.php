<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LogoutController extends Controller
{
    public function logout(Request $request) {
        $userTokens = $request->user()->tokens;
        foreach($userTokens as $token) {
            $token->revoke();   
        }

        return response('logged_out', 200);
    }
}
