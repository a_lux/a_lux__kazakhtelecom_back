<?php

namespace App\Http\Controllers\Api\Auth;

use App\User;
use App\Client;
use App\StationManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function registerClient(Request $request) {

        $validator = $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'company' => 'required|integer',
            'phone' => 'required|string',
            'address' => 'required|string',
            'password' => 'required|string|min:8|confirmed',
            'agreement' => 'required'
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role_id' => 2
        ]);

        $client = Client::create([
            'user_id' => $user->id,
            'name' => $request->name,
            'address' => $request->address,
            'phone' => $request->phone,
            'email' => $request->email,
            'company_id' => $request->company
        ]);
        
        $token = $user->createToken('Laravel Password Grant Client')->accessToken;

        $response = ['token' => $token, 'role_id' => $user->role_id, 'user' => $user];

        return response($response, 200);
    }

    public function registerStation(Request $request) {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'station' => 'required|integer',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role_id' => 5
        ]);

        StationManager::create([
            'user_id' => $user->id,
            'station_id' => $request->station
        ]);

        $token = $user->createToken('Laravel Password Grant Client')->accessToken;
        $response = ['token' => $token, 'role_id' => $user->role_id, 'user' => $user];

        return response($response, 200);
    }
}
