<?php

namespace App\Http\Controllers\Api\Ork;

use App\Driver;
use App\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\CompanyHistory;
use App\Http\Resources\DriverResource;
use App\Http\Resources\DriverCollection;

class DriverController extends Controller
{
    public function index(Request $request, Company $company)
    {
        $drivers = Driver::where('company_id', $company->id)->get();
        $columns = Driver::getColumns('ork');

        return response(['drivers' => new DriverCollection($drivers), 'columns' => $columns], 200);
    }
 
    public function show(Company $company, Driver $driver)
    {
        $driver = $driver->where('id', $driver->id)->where('company_id', $company->id)->first();
        $columns = Driver::getColumns('ork');

        return response(['driver' => new DriverResource($driver), 'columns' => $columns], 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'phone' => 'required|string',
            'company_id' => 'required|integer'
        ]);

        $driver = Driver::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'company_id' => $request->company_id
        ]);

        $company = Company::find($request->company_id);

        if($company->client) {
            $company->client->user->notify(new CompanyHistory([
                'type' => 'driver',
                'action' => 'create',
                'id' => $driver->id
            ]));
        }
        return response(new DriverResource($driver));
    }

    public function update(Request $request, Driver $driver)
    {
        $driver->update($request->all());
        $company = Company::find($request->company_id);

        if($company->client) {
            $company->client->user->notify(new CompanyHistory([
                'type' => 'driver',
                'action' => 'edit',
                'id' => $driver->id
            ]));
        }
        return new DriverResource($driver);
    }

    public function destroy(Request $request, Driver $driver)
    {
        $driver->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
