<?php

namespace App\Http\Controllers\Api\Ork;

use App\Region;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegionController extends Controller
{
    public function index() {
        
        return response(Region::get(), 200);
    }
}
