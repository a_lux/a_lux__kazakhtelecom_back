<?php

namespace App\Http\Controllers\Api\Ork;

use App\User;
use App\Order;
use App\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Templates\OrderTemplate;

class OrderController extends Controller
{
    public function show(Request $request, $id)
    {
        $order = $request
            ->user()
            ->orders()
            ->select([
                'orders.id',
                'clients.name as client_name',
                DB::raw('CONCAT(cars.brand, " ",cars.model) AS car_name'), 
                'cars.number as car_number', 
                'orders.location', 
                'orders.problem_description',
                'is_evacuated',
                'status_internal', 
                'stations.name as station_name', 
                'stations.address',
                'evacuators.name as evacuator_name',
                'evacuators.phone as evacuator_phone',
            ])
            ->join('cars', 'cars.id', 'orders.car_id')
            ->join('clients', 'clients.id', 'orders.client_id')
            ->leftJoin('stations', 'stations.id', 'orders.station_id')
            ->leftJoin('evacuators', 'evacuators.id', 'orders.evacuator_id')
            ->where('orders.id' ,$id)
            ->with('defectActs', 'defectActs.parts', 'defectActs.works', 'chatroom.participants', 'chatroom.messages')
            ->first();
        
        return response(['order' => $order], 200);
    }

    public function getOrders(Request $request, Order $order) {
        $orders = $request
            ->user()
            ->orders()
            ->select([
                'orders.id', 
                'clients.name as client_name',
                DB::raw('CONCAT(cars.brand, " ",cars.model) AS car_name'), 
                'cars.number as car_number', 
                'orders.location', 
                'orders.problem_description', 
                'is_evacuated', 
                'status_internal', 
                'stations.name as station_name', 
                'stations.address',
                'evacuators.name as evacuator_name',
                'evacuators.phone as evacuator_phone',
            ])
            ->join('cars', 'cars.id', 'orders.car_id')
            ->join('clients', 'clients.id', 'orders.client_id')
            ->leftJoin('stations', 'stations.id', 'orders.station_id')
            ->leftJoin('evacuators', 'evacuators.id', 'orders.evacuator_id')
            ->with('defectActs', 'defectActs.parts', 'defectActs.works');

        if($request->has('status_internal')) {
            $statuses = strpos($request->status_internal, ',') ? explode(',', $request->status_internal) : [$request->status_internal];
            $orders = $orders
                ->whereIn('status_internal', $statuses);
        }
        $orders = $orders->get();

        $colNames = Order::getColumns('ork');
        return response(['orders' => $orders, 'columns' => $colNames], 200);
    }

    public function onRepair(Request $request, Order $order) {
        $company = $order->client->company;
        $status_id = Status::where('name', 'Поиск партнера')->first()->id;
        $orpuManagerId = User::join('user_to_regions', 'user_id', 'users.id')
            ->select(['users.id'])
            ->where('role_id', 3)
            ->where('user_to_regions.region_id', $company->region_id)
            ->first()->id;
        
        OrderTemplate::init($order)
            ->changeStatus(1)
            ->changeStatus($status_id, false)
            ->setUser($orpuManagerId)
            ->getOrder();
        
        return response($order, 200);
    }

    public function buyParts(Request $request, Order $order) {
        $company = $order->client->company;
        $status_id = Status::where('name', 'Поиск автозапчестей')->first()->id;
        $orpaManagerId = User::join('user_to_regions', 'user_id', 'users.id')
            ->select(['users.id'])
            ->where('role_id', 4)
            ->where('user_to_regions.region_id', $company->region_id)
            ->first()->id;
        $order = OrderTemplate::init($order)
            ->changeStatus(2)
            ->changeStatus($status_id, false)
            ->setUser($orpaManagerId)
            ->getOrder();
        
        return response($order, 200);
    }
    
    public function approveDefectAct(Request $request, Order $order) {
        $status_id = Status::where('name', 'ДА ожидает согласование клиентом')->first()->id;
        if($request->input('while-repairing')) {
            $order = OrderTemplate::init($order)
                ->changeStatus(16)
                ->changeStatus($status_id, false)
                ->getOrder();
        }else {
            $order = OrderTemplate::init($order)
                ->changeStatus(10)
                ->changeStatus($status_id, false)
                ->getOrder();
        }
        
        return response(['status_changed' => $order->status_internal] ,200);
    }

    public function rejectDiagnoseAt(Order $order) {
        $order = OrderTemplate::init($order)
            ->changeStatus(6)
            ->getOrder();
        
        return response($order, 200);
    }
}