<?php

namespace App\Http\Controllers\Api\Ork;

use App\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\CompanyHistory;
use App\Http\Resources\CompanyResource;
use App\Http\Resources\CompanyCollection;

class CompanyController extends Controller
{
    public function index() {
        $columns = Company::getColumns('ork');
        $companies = Company::select([
            'companies.id',
            'companies.name',
            'companies.jur_address',
            'companies.fact_address',
            'companies.bin',
            'companies.bik',
            'companies.iik',
            'companies.contact_person',
            'companies.contact_phone',
            'regions.region_name as region',
            'companies.created_at',
            ])
            ->join('regions', 'regions.id', 'companies.region_id')
            ->get();
        
        return response(['companies' => new CompanyCollection($companies), 'columns' => $columns], 200);
    }

    public function show(Company $company) {
        return response(['company' => new CompanyResource($company)], 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'jur_address' => 'required|string',
            'fact_address' => 'required|string',
            'bin' => 'required|string',
            'bik' => 'required|string',
            'iik' => 'required|string',
            'contact_person' => 'required|string',
            'contact_phone' => 'required|string',
            'region_id' => 'required|integer'
        ]);

        return response(new CompanyResource(Company::create($request->all())), 200);
    }

    public function update(Request $request, Company $company) {
        $this->validate($request, [
            'name' => 'required|string',
            'jur_address' => 'required|string',
            'fact_address' => 'required|string',
            'bin' => 'required|string',
            'bik' => 'required|string',
            'iik' => 'required|string',
            'contact_person' => 'required|string',
            'contact_phone' => 'required|string',
            'region_id' => 'required|integer'
        ]);
        if($company->client) {
            $company->client->user->notify(new CompanyHistory([
                'type' => 'company',
                'action' => 'edit',
                'id' => $company->id
            ]));
        }

        $company->fill($request->all())->save();
        
        return response(new CompanyResource($company), 200);
    }
}
