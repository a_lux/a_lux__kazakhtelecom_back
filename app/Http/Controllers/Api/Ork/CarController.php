<?php

namespace App\Http\Controllers\Api\Ork;

use App\Car;
use App\Company;
use Illuminate\Http\Request;
use App\Http\Resources\CarResource;
use App\Http\Controllers\Controller;
use App\Http\Resources\CarCollection;
use App\Notifications\CompanyHistory;
 
class CarController extends Controller
{
    public function index(Request $request, Company $company)
    {
        $cars = Car::where('company_id', $company->id)->get();
        
        $columns = Car::getColumns('ork');
        return response(['cars' => new CarCollection($cars), 'columns' => $columns], 200);
    }
 
    public function show(Company $company, Car $car)
    {
        $car = $car->where('company_id', $company->id)->where('id', $car->id)->first();
        
        $columns = Car::getColumns('ork');
        return response(['car' => $car, 'columns' => $columns], 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [	
            'number' => 'required|string',
            'brand' => 'required|string',
            'model' => 'required|string',
            'company_id' => 'required|integer',
            'year' => 'required',
            'type_id' => 'required|integer'
        ]);
        
        $car = Car::create([
            'number' => $request->number,
            'brand' => $request->brand,
            'model' => $request->model,
            'company_id' => $request->company_id,
            'year' => $request->year,
            'type' => $request->type_id
        ]);
        
        $company = Company::find($request->company_id);

        if($company->client) {
            $company->client->user->notify(new CompanyHistory([
                'type' => 'car',
                'action' => 'create',
                'id' => $car->id
            ]));
        }

        return response($car, 200);
    }

    public function update(Request $request, Car $car)
    {
        $car->fill($request->all())->save();

        $company = Company::find($request->company_id);

        if($company->client) {
            $company->client->user->notify(new CompanyHistory([
                'type' => 'car',
                'action' => 'edit',
                'car' => $car->id
            ]));
        }
        
        return new CarResource($car);
    }
}
