<?php

namespace App\Http\Controllers\Api;

use App\Order;
use App\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Http\Resources\OrderCollection;
 
class OrderController extends Controller
{   
    public function show(Reqeust $request, Order $order)
    {
        return new OrderResource($order->load(['defectActs', 'comments', 'client', 'driver', 'car', 'evacuator', 'station']));
    }

    public function update(Request $request, Order $order)
    {
        $order->update($request->all());

        return new OrderResource($order);
    }

    public function destroy(Request $request, Order $order)
    {
        $order->delete();

        return response()->json([], \Illuminate\Http\Response::HTTP_NO_CONTENT);
    }
}
