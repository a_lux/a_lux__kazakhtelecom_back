<?php 

namespace App\Http\Templates;

use App\Media;
use App\Order;
use App\UserOrder;
use App\StationManager;
use Illuminate\Http\Request;

class OrderTemplate {
    private $order;

    static public function init(Order $order) {
        return new static($order);
    } 
    
    public function getOrder() {
        return $this->order;
    }
    public function changeStatus($status, $internal = true) {
        $data = $internal ? ['status_internal' => $status] : ['status' => $status];

        $this->order
            ->fill($data)
            ->save();
        
        return $this;
    }

    public function setUser($user_id) {
        UserOrder::create([
            'user_id' => $user_id,
            'order_id' => $this->order->id
        ]);
        
        return $this;
    }

    public function setOrUpdateUser($user_id) {
        if(UserOrder::where('user_id', $user_id)->where('order_id', $this->order->id)->exists()) {
           $userOrder = UserOrder::where('user_id', $user_id)->where('order_id', $this->order->id)->first();
           $userOrder->fill(['user_id' => $user_id])->save();
           
           return $this;
        }
        
        $this->setUser($user_id);
    }

    public function attachImages($images, $user_id, $manager_type) {
        foreach($images as $image) {
            $image = $image->store('order'.$this->order->id.'/images');
            Media::create([
                'link' => $image,
                'manager_type' => $manager_type,
                'manager_id' => $user_id,
                'order_id' => $this->order->id,
                'media_type' => 1
            ]);
        }

        return $this;
    }

    public function attachVideos($videos, $user_id, $manager_type) {
        foreach($videos as $video) {
            $video = $video->store('order'.$this->order->id.'/videos');
            Media::create([
                'link' => $video,
                'manager_type' => $manager_type,
                'manager_id' => $user_id,
                'order_id' => $this->order->id,
                'media_type' => 2
            ]);
        }

        return $this;
    }
    public function attachBill($bill, $user_id, $manager_type) {
        $bill = $bill->store('order'.$this->order->id.'/bills');
        Media::create([
            'link' => $bill,
            'manager_type' => $manager_type,
            'manager_id' => $user_id,
            'order_id' => $this->order->id,
            'media_type' => 3
        ]);
        
        return $this;
    }
    
    private function __construct(Order $order) {
        $this->order = $order;
    }
}