<?php

namespace App\Http\Templates;

use App\User;

class UserTemplate {
    public static function init() {
        return new static();
    }

    public function getUser($role_id, $region_id) {
        return User::join('user_to_regions', 'user_id', 'users.id')
            ->select(['users.id'])
            ->where('role_id', $role_id)
            ->where('user_to_regions.region_id', $region_id)
            ->first();
    }

    private function __construct() { 

    }
}