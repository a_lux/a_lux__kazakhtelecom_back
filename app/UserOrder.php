<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserOrder extends Pivot
{
    protected $fillable = ['user_id', 'order_id'];
}
