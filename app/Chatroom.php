<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chatroom extends Model
{
    protected $fillable = ['order_id'];
    
    public function messages() {
        return $this->hasMany(\App\Message::class);
    }

    public function participants() {
        return $this->hasMany(\App\Participant::class);
    }
}
