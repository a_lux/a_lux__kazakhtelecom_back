<?php

use App\UserToRegion;
use Illuminate\Database\Seeder;

class UserRegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(UserToRegion::class, 10)->create();
    }
}
