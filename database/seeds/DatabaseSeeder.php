<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(WorkSeeder::class);
        $this->call(MediaSeeder::class);
        $this->call(ShopSeeder::class);
        $this->call(RegionSeeder::class);
        $this->call(CarTypeSeeder::class);
        $this->call(StationSeeder::class);
        $this->call(PartSeeder::class);
        $this->call(EvacuatorSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(CompanySeeder::class);
        $this->call(DriverSeeder::class);        
        $this->call(UserSeeder::class);
        $this->call(CarSeeder::class);
        $this->call(UrlSeeder::class);
        $this->call(ClientSeeder::class);
        $this->call(OrderSeeder::class);
        $this->call(CommentSeeder::class);
        $this->call(DefectActSeeder::class);
        $this->call(StatusSeeder::class);
        $this->call(UserStatusSeeder::class);
        $this->call(UserToRegionSeeder::class);
    }
}