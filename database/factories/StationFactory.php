<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Station;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Station::class, function (Faker $faker) {
    return [
        'name' => $faker->name(),
        'city' => $faker->city(),
        'address' => $faker->address(),
        'fact_address' => $faker->address(),
        'jur_address' => $faker->address(),
        'bin' => random_int(11111111, 99999999)."",
        'bik' => random_int(11111111, 99999999)."",
        'iik' => random_int(11111111, 99999999)."",
        'contact_person' => $faker->name(),
        'contact_phone' => $faker->phoneNumber()
    ];
});
