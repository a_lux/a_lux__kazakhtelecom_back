<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\DefectAct;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(DefectAct::class, function (Faker $faker) {
    return [
        'order_id' => random_int(1, 10),
        'paid' => $faker->boolean(),
        'paid_at' => $faker->dateTimeBetween('-30 years', 'now'),
        'rejected' => $faker->boolean(),
        'rejected_at' => $faker->dateTimeBetween('-30 years', 'now'),
    ];
});
