<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Contract;
use Faker\Generator as Faker;

$factory->define(Contract::class, function (Faker $faker) {
    return [
        'company_id' => random_int(1, 10),
        'number_of_сontract' => $faker->phoneNumber(),
        'signed_at' => $faker->dateTimeBetween('-30 years', 'now'),
        'expire_at'	=> $faker->dateTimeBetween('-30 years', 'now')
    ];
});
