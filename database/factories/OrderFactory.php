<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Order;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Order::class, function (Faker $faker) {
    return [
        'car_id' => random_int(1, 10),
        'client_id' => random_int(1, 10),
        'is_evacuated' => $faker->boolean(),
        'location' => $faker->address(),
        'actual_location' => $faker->address(),
        'driver_id' => random_int(1, 10),
        'problem_description' => $faker->sentence(),
        'status' => random_int(1, 10),
        'status_internal' => random_int(1, 10),
        'ready_to_repair_at' => $faker->dateTimeBetween('-30 years', 'now'),
        'station_id' => random_int(1, 5),
        'completed_at' => $faker->dateTimeBetween('-30 years', 'now'),
        'evacuator_id' => random_int(1, 10)
    ];
});
