<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\UserStatus;
use Faker\Generator as Faker;

$factory->define(UserStatus::class, function (Faker $faker) {
    return [
        'status_id' => random_int(1, 10),
        'role_id' => random_int(1, 10)
    ];
});
